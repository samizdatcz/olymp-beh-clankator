var names = {
	'101662': 'jan czech',
	'103268': 'luboš gaisl',
	'103409': 'michal portales',
	'103502': 'lukáš vodička',
	'103573': 'tomáš hradecký',
	'103670': 'václav blahout',
	'103755': 'anna outratová',
	'103823': 'jan trčka',
	'103916': 'iveta kmetková',
	'104005': 'jiří urban',
	'104077': 'martina špatná',
	'104183': 'iva krupíková',
	'104306': 'ondřej škaroupka',
	'104481': 'zuzana zemanová',
	'104584': 'petra hudíková',
	'104764': 'jindřich barabáš',
	'104883': 'ilona líkařová',
	'105098': 'martin belica',
	'105254': 'petr maleček',
	'105351': 'renata ryšavá',
	'105513': 'lukáš červenka',
	'105642': 'karel štrouf',
	'105772': 'jiří beran',
	'105984': 'petra feřtrová',
	'106098': 'tomáš hrubý',
	'106251': 'jan třasák',
	'106365': 'klára steinbauerová',
	'106587': 'tomáš grosman'
};

var runnerId = window.location.href.split('#')[1]

//ptže IE: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith
if (!String.prototype.endsWith) {
  String.prototype.endsWith = function(searchString, position) {
      var subjectString = this.toString();
      if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.lastIndexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
  };
}

function getSex(surname) {
	if (surname.endsWith('á')) {
		return ['překvapila', 'která', 'poskytla'];
	} else {
		return ['překvapil', 'který', 'poskytl'];
	};
};

if (runnerId) {
	var runnerName = names[parseInt(runnerId)];
	var pers = getSex(runnerName);

	var foto = '<div class="b-inline b-inline--left hide--m"><div class="b-inline__wrap"><div class="b-inline__content"><blockquote class="text-serif"><p><img class="runph" style="width:238px; height: auto; image-orientation: from-image;" src="https://olympicrun.s3.amazonaws.com/photos_pub/' + runnerId + '.jpg"></p></blockquote></div></div>'

	var stuffing = 'Svým výsledkem ' + pers[0] + ' i <span style="text-transform:capitalize;">' + runnerName + '</span>, ' + pers[1] + ' po úspěšném zakončení běhu ' + pers[2] + ' Radiožurnálu krátký rozhovor. <br><br><i>Poslechněte si celou reportáž</i><br><audio controls><source src="https://olympicrun.s3.amazonaws.com/public/' + runnerId + '.mp3" type="audio/mpeg"> Váš prohlížeč nepodporuje přehrávání audia.</audio>'

	$('.cont').html(stuffing);
	$('.runnerphoto').html(foto);
}
else {
	$('.cont').remove();	
}


